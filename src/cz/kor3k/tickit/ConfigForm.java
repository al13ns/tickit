package cz.kor3k.tickit;

import javax.swing.*;
import java.text.ParseException;

/**
 * Created by kor3k on 26.09.2015.
 */
public class ConfigForm {
    private JSlider players;
    private JSlider ticks;
    private JSpinner boardX;
    private JSpinner boardY;
    private JButton playButton;
    private JPanel content;
    private JSpinner resX;
    private JSpinner resY;

    private void createUIComponents()
    {
        boardX  =   new JSpinner( new SpinnerNumberModel( 5 , 5 , 100 , 1 ) );
        boardY  =   new JSpinner( new SpinnerNumberModel( 5 , 5 , 100 , 1 ) );
        resX    =   new JSpinner( new SpinnerNumberModel( 10 , 10 , 1000 , 10 ) );
        resY    =   new JSpinner( new SpinnerNumberModel( 10 , 10 , 1000 , 10 ) );

        playButton  =   new JButton();
        playButton.addActionListener(e -> onPlay());

        //(Integer) spinner.getValue();
        //content.getRootPane().setDefaultButton( playButton );
    }

    public void onPlay()
    {}

    public static void main(String[] args)
    {
        JFrame frame = new JFrame("ConfigForm");
        frame.setContentPane(new ConfigForm().content);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public JPanel getContentPanel()
    {
        return content;
    }

    public JButton getPlayButton()
    {
        return playButton;
    }

    private int getBoardWidth()
    {
        try {
            boardX.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (int) boardX.getValue();
    }

    private int getBoardHeight()
    {
        try {
            boardY.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (int) boardY.getValue();
    }

    private int getWidth()
    {
        try {
            resX.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (int) resX.getValue();
    }

    private int getHeight()
    {
        try {
            resY.commitEdit();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return (int) resY.getValue();
    }

    public void getData(Config data)
    {
        data.setBoardHeight( this.getBoardHeight() );
        data.setBoardWidth( this.getBoardWidth() );
        data.setHeight( this.getHeight() );
        data.setWidth( this.getWidth() );
        data.setPlayers( players.getValue() );
        data.setTicksToWin( ticks.getValue() );
    }

    public void setData(Config data)
    {
        resY.setValue( new Integer( data.getHeight() ) );
        resX.setValue( new Integer( data.getWidth() ) );
        boardY.setValue( new Integer( data.getBoardHeight() ) );
        boardX.setValue( new Integer( data.getBoardWidth() ) );
        players.setValue( data.getPlayers() );
        ticks.setValue( data.getTicksToWin() );
    }
}
