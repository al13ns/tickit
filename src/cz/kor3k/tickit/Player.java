package cz.kor3k.tickit;

import java.awt.*;

/**
 * Created by kor3k on 14.09.2015.
 */
public class Player
{
    private String name;
    private Color color;

    public Player( Color color , String name )
    {
        this.color  =  color;
        this.name   =  name;
    }

    public Color getColor()
    {
        return this.color;
    }

    public String getName()
    {
        return this.name;
    }
}
