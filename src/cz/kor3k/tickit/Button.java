package cz.kor3k.tickit;

import javax.swing.JButton;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by kor3k on 14.09.2015.
 */
public class Button extends JButton implements ActionListener
{
    public  Player      owner = null;
    private Game   p;

    private static Font font   =   new Font("Arial", Font.PLAIN, 10);

    public Button( Game p )
    {
        this.addActionListener( this );
        this.setFont( font );
        this.p  =   p;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
        p.makeMove( this );
    }
}
