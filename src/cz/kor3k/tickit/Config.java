package cz.kor3k.tickit;

public class Config implements java.io.Serializable
{
    private int players;
    private int ticksToWin;
    private int width;
    private int height;
    private int boardWidth;
    private int boardHeight;

    public Config()
    {
        players     =   2;
        ticksToWin  =   5;
        width       =   600;
        height      =   600;
        boardWidth  =   15;
        boardHeight =   15;
    }

    public int getPlayers() {
        return players;
    }

    public void setPlayers(int players) {
        this.players = players;
    }

    public int getTicksToWin() {
        return ticksToWin;
    }

    public void setTicksToWin(int ticksToWin) {
        this.ticksToWin = ticksToWin;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBoardWidth() {
        return boardWidth;
    }

    public void setBoardWidth(int boardWidth) {
        this.boardWidth = boardWidth;
    }

    public int getBoardHeight() {
        return boardHeight;
    }

    public void setBoardHeight(int boardHeight) {
        this.boardHeight = boardHeight;
    }

//    public boolean equals(Object other) {
//        return (other instanceof User) && (id != null) ? id.equals(((User) other).id) : (other == this);
//    }
//    public int hashCode() {
//        return (id != null) ? (getClass().hashCode() + id.hashCode()) : super.hashCode();
//    }
//    public String toString() {
//        return String.format("User[id=%d,name=%s,birthdate=%d]", id, name, birthdate);
//    }
}
