package cz.kor3k.tickit;

import javax.swing.*;
import java.awt.Color;

/**
 * Created by kor3k on 27.09.2015.
 */
public class Info {
    private JTextField turns;
    private JButton resetButton;
    private JPanel content;
    private JTextField boardWidth;
    private JTextField boardHeight;
    private JTextField ticksToWin;
    private JTextField status;
    private JButton newButton;
    private JButton exitButton;

    public void setData(Game game)
    {
        turns.setText( String.valueOf( game.getTurns() ) );
        boardWidth.setText( String.valueOf( game.getConfig().getBoardWidth() ) );
        boardHeight.setText( String.valueOf( game.getConfig().getBoardHeight() ) );
        ticksToWin.setText( String.valueOf( game.getConfig().getTicksToWin() ) );

        status.setText( "Player " + game.getCurrentPlayer().getName() + " turns" );
        status.setForeground( game.getCurrentPlayer().getColor() );
    }

    public void winner( Player winner )
    {
        status.setText( "Player " + winner.getName() + " WINS!" );
        status.setForeground( winner.getColor() );
    }

    public void draw()
    {
        status.setText( "Game ends in a DRAW!" );
        status.setForeground( new Color( 0 , 0 , 0 ) );
    }

    public void getData(Game data)
    {}

    public JPanel getContentPanel()
    {
        return content;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Info");
        frame.setContentPane(new Info().content);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents()
    {
        resetButton =   new JButton();
        resetButton.addActionListener(e -> onReset());

        newButton   =   new JButton();
        newButton.addActionListener(e -> onNew());

        exitButton  =   new JButton();
        exitButton.addActionListener(e -> onExit());
    }

    public void onReset()
    {}

    public void onNew()
    {}

    public void onExit()
    {}
}
