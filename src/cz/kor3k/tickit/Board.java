package cz.kor3k.tickit;

/**
 * Created by kor3k on 14.09.2015.
 */
public class Board
{
    private int width , height;

    private Button[][]  buttons;
    private Game   p;


    public Board( int width , int height , Game p )
    {
        this.width  =   width;
        this.height =   height;
        this.p      =   p;

        this.buttons    =   new Button[width][height];

        for( int x = 0 ; x < width ; x++ )
        {
            for( int y = 0 ; y < height ; y++ )
            {
                buttons[x][y] = new Button(p);
            }
        }

    }

    public Button[][] getButtons()
    {
        return this.buttons;
    }

    public int getSize()
    {
        if( 0 == buttons.length || 0 == buttons[0].length )
        {
            return 0;
        }
        else
        {
            return buttons.length * buttons[0].length;
        }
    }

    public int getWidth()
    {
        return this.width;
    }

    public int getHeight()
    {
        return this.height;
    }

    public void disable()
    {
        for( Button[] b1 : buttons )
        {
            for( Button b : b1 )
            {
                b.setEnabled( false );
            }
        }
    }

}
