package cz.kor3k.tickit;

import javax.swing.*;

/**
 * Created by kor3k on 27.09.2015.
 */
public class TickIt
{
    public final static int[]  colors  =   new int[]{
            0xaa0000 ,
            0x00aa00 ,
            0x0000aa ,
            0xaa00aa ,
            0xaaaa00 ,
            0x00aaaa ,
            0x000000 ,
            0xaaaaaa ,
            0x00aaff ,
            0xff00aa
    };

    private static JFrame game;
    private static JFrame info;

    public static void main(String[] args)
    {
        //timeout (20)

        startConfig();
    }

    private static void startConfig()
    {
        Config      config          =   new Config();
        JFrame      configFrame     =   new JFrame( "TickIt! Config" );
        ConfigForm  configForm      =   new ConfigForm()
        {
            public void onPlay()
            {
                this.getData( config );
                configFrame.dispose();
                startGame( config );
            }
        };
        configForm.setData( config );

        configFrame.getRootPane().setDefaultButton( configForm.getPlayButton() );
        configFrame.setContentPane( configForm.getContentPanel() );
        configFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        configFrame.pack();
        configFrame.setVisible(true);
    }

    private static void startGame( Config config )
    {
        JFrame  infoFrame   =   new JFrame( "TickIt! Info" );
        Info    infoForm    =   new Info()
        {
            public void onReset()
            {
                TickIt.game.dispose();
                TickIt.info.dispose();
                startGame( ((Game)TickIt.game).getConfig() );
            }

            public void onNew()
            {
                TickIt.game.dispose();
                TickIt.info.dispose();
                startConfig();
            }

            public void onExit()
            {
                System.exit(0);
            }
        };
        Game    game        =   new Game( config )
        {
            public void endTurn()
            {
                Player winner   =   this.checkWinner();
                if( null != winner )
                {
                    turns++;
                    infoForm.setData( this );
                    infoForm.winner( winner );
                    this.board.disable();
                    TickIt.game.setTitle( "TickIt! - Game Won" );
                }
                else if( this.checkDraw() )
                {
                    turns++;
                    infoForm.setData( this );
                    infoForm.draw();
                    this.board.disable();
                    TickIt.game.setTitle( "TickIt! - Game Drawn" );
                }
                else
                {
                    super.endTurn();
                    infoForm.setData( this );
                }
            }
        };

        infoForm.setData( game );

        infoFrame.setContentPane( infoForm.getContentPanel() );
        infoFrame.setDefaultCloseOperation( JFrame.HIDE_ON_CLOSE );
        infoFrame.pack();
        infoFrame.setVisible(true);

        TickIt.game   =   game;
        TickIt.info   =   infoFrame;
    }
}
