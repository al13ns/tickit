package cz.kor3k.tickit;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Color;
import java.awt.GridLayout;

/**
 * Created by kor3k on 27.09.2015.
 */
public class Game extends JFrame
{
    private final   Config      config;
    private         JPanel      panel;
    private         int         toWin;
    private         Player[]    players;
    private         int         currentPlayer;
    protected       Board       board;
    protected       int         turns;


    public Game( Config config)
    {
        super( "TickIt!" );
        this.config     =   config;
        this.panel      =   new JPanel();
        create();
    }

    private void create()
    {
        turns  =   0;
        toWin  =   config.getTicksToWin();

        int[] players    =   new int[config.getPlayers()];
        for( int i=0 ; i < config.getPlayers() ; i++ )
        {
            players[i]   =   TickIt.colors[i];
        }

        setSize( config.getWidth() , config.getHeight() );
        setResizable( false );
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        panel.setLayout( new GridLayout( config.getBoardWidth() , config.getBoardHeight() ) );

        initPlayers( players );
        initBoard( config.getBoardWidth() , config.getBoardHeight() );

        add( panel );
        setVisible( true );
    }

    private void initPlayers( int[] players )
    {
        this.players    =   new Player[players.length];

        for( int i=0 ; i < players.length ; i++ )
        {
            this.players[i] =   new Player( new Color( players[i] ) , String.valueOf(i) );
        }

        this.currentPlayer   =   0;
    }

    private void initBoard( int width , int height )
    {
        this.board  =   new Board( width , height , this );

        for( Button[] b1 : board.getButtons() )
        {
            for( Button b : b1 )
            {
                panel.add(b);
            }
        }
    }

    public void makeMove( Button button )
    {
        if( button.owner != null )
        {
            return;
        }

        button.owner  =   this.getCurrentPlayer();
//        button.setText( String.valueOf( button.owner.getSign() ) );
        button.setBackground( button.owner.getColor() );


        this.endTurn();
    }

    public void endTurn()
    {
        Player winner   =   this.checkWinner();
        if( null != winner )
        {
            System.out.println( "WINS Player " + winner.getName() + " at turn " + turns  );
            //players wins
        }

        if( this.checkDraw() )
        {
            System.out.println( "DRAW" );
            //draw
        }

        if( ++currentPlayer >= players.length )
        {
            currentPlayer   =   0;
        }

        turns++;
    }

    protected boolean checkDraw()
    {
        //if only one left (or less)
        if( turns >= board.getSize()-2 )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected Player checkWinner()
    {
        int row = 0 , col = 0 , i = 0;
        Button[][] buttons  =   board.getButtons();

        for( row = 0; row < board.getWidth(); row++ )                       // This first for loop checks every row
        {
            for( col = 0; col < (board.getHeight()-(toWin-1)); col++)           // And all columns until N away from the end
            {
                while(buttons[row][col].owner == getCurrentPlayer() )      // For consecutive rows of the current player's mark
                {
                    col++;
                    i++;
                    if(i == toWin)
                    {
                        return getCurrentPlayer();
                    }
                }
                i = 0;
            }
        }

        for( col = 0; col < board.getHeight(); col++ )                       // This one checks for columns of consecutive marks
        {
            for( row = 0; row < (board.getWidth()-(toWin-1)); row++ )
            {
                while( buttons[row][col].owner == getCurrentPlayer() )
                {
                    row++;
                    i++;
                    if (i == toWin)
                    {
                        return getCurrentPlayer();
                    }
                }
                i = 0;
            }
        }

        for( col = 0; col < (board.getHeight() - (toWin-1)); col++)             // This one checks for "forwards" diagonal runs
        {
            for( row = 0; row < (board.getWidth()-(toWin-1)); row++ )
            {
                while( buttons[row][col].owner == getCurrentPlayer() )
                {
                    row++;
                    col++;
                    i++;
                    if (i == toWin)
                    {
                        return getCurrentPlayer();
                    }
                }
                i = 0;
            }
        }

        // Finally, the backwards diagonals:
        for( col = board.getHeight()-1; col > 0+(toWin-2); col--)           // Start from the last column and go until N columns from the first
        {                                                   // The math seems strange here but the numbers work out when you trace them
            for( row = 0; row < (board.getWidth()-(toWin-1)); row++)       // Start from the first row and go until N rows from the last
            {
                while( buttons[row][col].owner == getCurrentPlayer() )  // If the current player's character is there
                {
                    row++;                                  // Go down a row
                    col--;                                  // And back a column
                    i++;                                    // The i variable tracks how many consecutive marks have been found
                    if (i == toWin)                             // Once i == N
                    {
                        return getCurrentPlayer();                      // Return the current player number to the
                    }                                       // winnner variable in the playGame function
                }                                           // If it breaks out of the while loop, there weren't N consecutive marks
                i = 0;                                      // So make i = 0 again
            }                                               // And go back into the for loop, incrementing the row to check from
        }

        return null;
    }

    public Config getConfig()
    {
        return this.config;
    }

    public int getTurns()
    {
        return this.turns;
    }

    public Player getCurrentPlayer()
    {
        return players[currentPlayer];
    }
}
