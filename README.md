# TickIt! #

Customizable XO/TicTacToe Implementation

### You can specify ###

* number of players *(2-5)*
* ticks needed to win *(3-10)*
* rectangle board size *(5-100)*
* rectangle board resolution *(10-1000)*

------------------

![1.png](https://bitbucket.org/repo/bbxXXe/images/500274227-1.png)

![2.png](https://bitbucket.org/repo/bbxXXe/images/270529193-2.png)

![3.png](https://bitbucket.org/repo/bbxXXe/images/1205050077-3.png)